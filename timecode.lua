-- Timecode parsing
function parse_timecode(timecode_string)
	if timecode_string == "" or timecode_string == nil then
		return nil
	end
	timecode = {}
	for field in string.gmatch(timecode_string, "([^:]+)") do
                if timecode["hour"] == nil then
                	timecode["hour"] = tonumber(field, 10)
                elseif timecode["minute"] == nil then
                	timecode["minute"] = tonumber(field, 10)
                elseif timecode["second"] == nil then
                	timecode["second"] = tonumber(field, 10)
                elseif timecode["frame"] == nil then
                	timecode["frame"] = tonumber(field, 10)
                end
        end
        if timecode["hour"] == nil or timecode["minute"] == nil or timecode["second"] == nil or timecode["frame"] == nil then
        	return nil
        end
	return timecode
end

-- Timecode formatting
function format_timecode(timecode)
	if timecode == nil then
		return "nil"
	end

	timecode_string = ""
	timecode_string = timecode_string .. string.format("%02d", timecode.hour) .. ":"
	timecode_string = timecode_string .. string.format("%02d", timecode.minute) .. ":"
	timecode_string = timecode_string .. string.format("%02d", timecode.second) .. ":"
	timecode_string = timecode_string .. string.format("%02d", timecode.frame)
	return timecode_string
end

-- Timecode visibility
timecode_visible = false
function toggle_timecode()
	timecode_visible = not timecode_visible
	print("Timecode visibility changed to " .. tostring(timecode_visible))
	update_timecode()
end
mp.add_key_binding("a", toggle_timecode)

-- Timecode metadata
start_timecode = parse_timecode(mp.get_property("metadata/by-key/timecode"))

function update_start_timecode()
	start_timecode = parse_timecode(mp.get_property("metadata/by-key/timecode"))
	timecode_visible = start_timecode and start_timecode ~= nil
	print("File loaded with start timecode " .. format_timecode(start_timecode) .. " - timecode visibility set to " .. tostring(timecode_visible))
	update_timecode()
end
mp.register_event("file-loaded", update_start_timecode)

-- Timecode UI
timecode_ui = nil
function update_timecode(name, playback_time)
	if not timecode_visible or start_timecode == nil then
		if timecode_ui ~= nil then
			timecode_ui:remove()
			timecode_ui = nil
		end
		return
	end
	if timecode_ui == nil then
		timecode_ui = mp.create_osd_overlay("ass-events")
	end
	if playback_time == nil then
		playback_time = mp.get_property_number("playback-time")
	end
	
	local fps = mp.get_property_number("container-fps")
	if fps == nil or fps < 20 or fps > 30 then
		fps = mp.get_property_number("estimated-vf-fps")
		if fps == nil then
			return
		end
	end
	fps = tonumber(string.format("%.0f", fps))
	
	local current_timecode = {}
	current_timecode["frame"] = start_timecode["frame"] + tonumber(string.format("%.0f", playback_time % 1 * fps)) - 1
	current_timecode["second"] = start_timecode["hour"] * 3600 + start_timecode["minute"] * 60 + start_timecode["second"] + math.floor(playback_time) + math.floor(current_timecode["frame"] / fps)
	current_timecode["minute"] = math.floor(current_timecode["second"] / 60)
	current_timecode["hour"] = math.floor(current_timecode["minute"] / 60)
	current_timecode["frame"] = current_timecode["frame"] % fps
	current_timecode["second"] = current_timecode["second"] % 60
	current_timecode["minute"] = current_timecode["minute"] % 60

	timecode_ui.data = "{\\an1}{\\pos(60,670)}{\\b1}" .. format_timecode(current_timecode)
	timecode_ui:update()
end
mp.observe_property("playback-time", "number", update_timecode)

