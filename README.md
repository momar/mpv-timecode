# mpv Timecode Scripts

This repository contains scripts for the media player [mpv](https://mpv.io/), built to make screening professional video footage under Linux easy.

![](https://i.vgy.me/2mIil4.jpg)

It includes a Lua script `timecode.lua`, which, if copied to `~/.config/mpv/scripts/`, adds the timecode of MXF files to mpv's OSD. The timecode can be toggled by pressing `a`.

The script `open-mxf-folder` can be copied to Nautilus' script directory `~/.local/share/nautilus/scripts/` to add an option to the context menu that lets you search a folder for MXF files and open them with mpv, sorted by timecode.

To quickly copy all files to the correct destination, clone the repository with `git clone https://codeberg.org/momar/mpv-timecode.git && cd mpv-timecode`, and then run `make install`.

<!--If you want to install the great mpv plugins [gallery view](https://github.com/occivink/mpv-gallery-view), [autoload](https://github.com/mpv-player/mpv/blob/master/TOOLS/lua/autoload.lua) and the [thumbnail script](https://github.com/TheAMM/mpv_thumbnail_script), just run `make install-plugins` in the same folder.  
This will also add MXF files to the autoload script & set the thumbnail dir to `~/.config/mpv/gallery_thumbs_dir/`. You can then list preview a list of files in the current playlist/directory with `g`.-->
