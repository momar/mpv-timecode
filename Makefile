help:
	echo "Run 'make install' to install the scripts to the correct directories."

install:
	mkdir -p ~/.config/mpv/scripts/
	cp timecode.lua ~/.config/mpv/scripts/
	mkdir -p ~/.local/share/nautilus/scripts/
	cp open-mxf-folder ~/.local/share/nautilus/scripts/"Open all contained MXF files"
